//
// Projectile model
//

// Create new projectile type
function ProjectileType(config) {

  this.strength = config.strength;
  this.mass = config.mass;
  this.range = config.range;
  this.rechargeTime = config.rechargeTime;
  
  this.collisionGroup = config.collisionGroup;
  
  this.sprite = new Sprite(config.spriteURL);
}


	
// Create new bullet projectile instance
function Bullet(config) {
    
	var self = this;
  
	this.scale = Game.config.bullet_scale;
    
	this.range = Game.config.bullet_lifespan; // How long the bullet lasts

	this.mBody = Matter.Bodies.circle(config.pos.x, config.pos.y, Game.config.sprite_image_width);
  
	this.mBody.collisionFilter.group = config.collisionGroup;
	this.mBody.collisionFilter.category = Game.Model.CollisionModel.Projectile.Category;
	this.mBody.collisionFilter.mask = Game.Model.CollisionModel.Projectile.Mask;
	  
  this.mBody.mass = config.type.mass;
  this.mBody.frictionAir = 0;
  Matter.Body.applyForce(this.mBody, config.pos, config.direction);
   
    // Event handlers for current state
	this.postUpdate = config.postUpdate;
  
  
  
	this.draw = function(context) {
    
		var pos = this.mBody.position;
		//Matter.Bodies.circle(1,1,10);
		config.type.sprite.draw(context, pos.x, pos.y, this.scale);
	}

  
  // Collision interface
  
	this.doCollision = function(otherBody, env) {
	  
		otherBody.collideWithProjectile(this, env);
	}
  
	this.collideWithPlayer = function(player, env) {
    
		player.collideWithProjectile(this, env);
	}
  
	this.collideWithProjectile = function(otherProjectile, env) {
	}
  
	this.collideWithNPC = function(npc, env) {
    
		npc.collideWithProjectile(this, env);
	}
	
	this.addToWorld = function(world) {
    
       Matter.World.add(world,this.mBody);
    }
	
}

