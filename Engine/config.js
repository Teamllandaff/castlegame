
'use strict';

//
// Engine configuration
//

Game = (function(module) {

  module.config = {

    playerName : 'Player 1',

    player_mass : 18,

    player_move_speed : 6,

    player_top_speed : 6,

    player_jump_speed : 8, //was 8
    
    player_sprite_scale : 1,

    player_controls : { moveLeft : 'LEFT', moveRight : 'RIGHT', jump : 'SPACE', shoot: 'B' },
    
    
    pickup_sprite_scale : 0.75, // Bigger number = larger sprite!

    pickup_time_delay : 5, // Delay (in seconds) between pickups appearing

    frictionCoeff : 0.025,
    
    default_creature_mass : 18,
    default_creature_sprite_scale : 1,
    default_creature_damage : 10,
	creature_move_speed : 2,
    
    default_platform_mass : 20,
    default_platform_sprite_scale : 1,
	
	projectile_speed : 0.0009, // Bigger number = faster!
	
	bulletRechargeTime : 1, // Smaller number = faster fire rate
	
	bullet_scale : 1.5, // Bigger number = larger sprite

	bullet_lifespan : 200, // Bigger number = longer range
	bullet_sprite_scale : 2.5, // Bigger number = larger sprite!
	sprite_image_width:20,
    
    show_bounding_volume : true
  };
  
  
  return module;
  
})((window.Game || {}));

