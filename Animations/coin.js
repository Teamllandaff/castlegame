Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});



	module.spritesheet['Coin'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/Coin.png',
		frameWidth : 32,
		frameHeight : 32,
		framesPerSecond : 5
	});


	module.sequence['Coin'] = (module.sequence['Coin'] || {});
	module.sequence['Coin']['doing_stuff'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['Coin'],
		startFrame : 0,
		endFrame : 1,
		oscillate : true,
		flipHorizontal : false,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));